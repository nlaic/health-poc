# federated-learning-demo
Helm chart for a Federated Learning demo

## Requirements
- A running kubernetes cluster
- Kubectl configured to use the cluster
- Helm 3

## Deployment content
This umbrella chart deploys the following components using Helm charts:
- IDS broker
- IDS DAPS
- MongoDb instance (used by the broker and DAPS)
- IDS connectors for Alice, Bob, Charlie and Researcher.

>*NOTE:* All component charts are included as local dependencies (`charts/*.tgz`) for simplicity.

### Install command
Make sure to install the Helm chart using
```
helm install federated-learning . -f values.yaml -f values.alice.yaml -f values.bob.yaml -f values.charlie.yaml -f values.researcher.yaml
```
to install the connectors for Alice, Bob, Charlie and Researcher.

### IDS Connectors
Each connector is deployed as a dependency described in ``Chart.yaml``, where an alias is used to give it it's instance name.
For each of those instances, a separate values file (like `values.alice.yaml`) is present.
Remove a dependency in `Chart.yaml` to deploy a subset of connectors.

## Configuration values
Most of the values present in the `values*.yaml` files are configured so that all services are connected.
The following values are important to change:
- **mongoDbPassword**: password used to connect to the mongodb instance
- **host**: (in each of the `values.*.yaml`) URL to reach a specific connector.

A table containing all default values is shown below:
Note that only `Alice's` values are displayed.
Bob, Charlie and Researche use similar configuration values.

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| alice.coreContainer.image | string | `"registry.ids.smart-connected.nl/trusted-core-container:2.0.0-develop-nonroot-202010191155"` |  |
| alice.coreContainer.replicaCount | int | `1` |  |
| alice.deployment.pullPolicy | string | `"Always"` |  |
| alice.host | string | `"alice.health.ia2ai.nl"` |  |
| alice.ids.containerManager.type | string | `"none"` |  |
| alice.ids.containers[0].config.customProperties.client | bool | `true` |  |
| alice.ids.containers[0].config.customProperties.datasetEndpoint | string | `"http://{{.Release.Name}}-{{.Chart.Name}}-worker:8080/datasets"` |  |
| alice.ids.containers[0].config.customProperties.forward | string | `"ws://{{.Release.Name}}-{{.Chart.Name}}-worker:8777"` |  |
| alice.ids.containers[0].config.customProperties.queryEndpoint | string | `"http://{{.Release.Name}}-{{.Chart.Name}}-fuseki:3030/connectorData"` |  |
| alice.ids.containers[0].config.customProperties.resourcePath | string | `"/datasets/"` |  |
| alice.ids.containers[0].config.customProperties.updateEndpoint | string | `"http://{{.Release.Name}}-{{.Chart.Name}}-fuseki:3030/connectorData"` |  |
| alice.ids.containers[0].config.id | string | `"urn:ids:connectors:alice"` |  |
| alice.ids.containers[0].config.participant | string | `"urn:ids:participants:TNO"` |  |
| alice.ids.containers[0].config.selfDescriptionConfig.type | string | `"none"` |  |
| alice.ids.containers[0].image | string | `"registry.ids.smart-connected.nl/federated-learning:open-source-202011180841"` |  |
| alice.ids.containers[0].name | string | `"federated-learning"` |  |
| alice.ids.containers[0].persistentVolume.mountPath | string | `"/datasets"` |  |
| alice.ids.containers[0].persistentVolume.name | string | `"alice-datasets"` |  |
| alice.ids.containers[0].persistentVolume.storageSize | string | `"1Gi"` |  |
| alice.ids.containers[0].type | string | `"data-app"` |  |
| alice.ids.containers[1].environment.WORKER_ID | string | `"{{.Release.Name}}-{{.Chart.Name}}"` |  |
| alice.ids.containers[1].image | string | `"registry.ids.smart-connected.nl/pysyft-worker:open-source"` |  |
| alice.ids.containers[1].name | string | `"worker"` |  |
| alice.ids.containers[1].type | string | `"helper"` |  |
| alice.ids.containers[2].environment.API_BACKEND | string | `"{{.Release.Name}}-{{.Chart.Name}}-federated-learning:8080/api/"` |  |
| alice.ids.containers[2].image | string | `"registry.ids.smart-connected.nl/vodan-worker-gui:master-202011120916"` |  |
| alice.ids.containers[2].name | string | `"gui"` |  |
| alice.ids.containers[2].type | string | `"helper"` |  |
| alice.ids.containers[3].environment.ADMIN_PASSWORD | string | `"SPARQL_TEST"` |  |
| alice.ids.containers[3].image | string | `"registry.ids.smart-connected.nl/fuseki:master"` |  |
| alice.ids.containers[3].name | string | `"fuseki"` |  |
| alice.ids.containers[3].type | string | `"helper"` |  |
| alice.ids.daps.url | string | `"http://{{.Release.Name}}-daps:8080"` |  |
| alice.ids.daps.uuid | string | `"ALICE-TEST-UUID"` |  |
| alice.ids.info.accessUrl | string | `"http://{{ .Release.Name }}-{{ .Chart.Name }}:8080/router"` |  |
| alice.ids.info.broker.address | string | `"http://{{.Release.Name}}-broker:8080/infrastructure"` |  |
| alice.ids.info.broker.autoRegister | bool | `true` |  |
| alice.ids.info.broker.id | string | `"urn:ids:connectors:Broker"` |  |
| alice.ids.info.broker.profile | string | `"FULL"` |  |
| alice.ids.info.curator | string | `"urn:ids:participants:TNO"` |  |
| alice.ids.info.descriptions[0] | string | `"Federated Learning worker connector"` |  |
| alice.ids.info.idsid | string | `"urn:ids:connectors:alice"` |  |
| alice.ids.info.maintainer | string | `"urn:ids:participants:TNO"` |  |
| alice.ids.info.titles[0] | string | `"Alice FL"` |  |
| alice.ids.keystore.cert | string | `""` |  |
| alice.ids.keystore.key | string | `""` |  |
| alice.ids.logLevel | string | `"INFO"` |  |
| alice.ids.routes[0].clearing | bool | `false` |  |
| alice.ids.routes[0].dapsInject | bool | `false` |  |
| alice.ids.routes[0].type | string | `"HTTPS-out"` |  |
| alice.ids.routes[1].clearing | bool | `false` |  |
| alice.ids.routes[1].dapsVerify | bool | `false` |  |
| alice.ids.routes[1].endpoint | string | `"router"` |  |
| alice.ids.routes[1].router | string | `"http://{{.Release.Name}}-{{.Chart.Name}}-federated-learning:8080/router"` |  |
| alice.ids.routes[1].type | string | `"HTTPS-in"` |  |
| alice.ids.tpm.createSimulator | bool | `false` |  |
| alice.ids.truststore.chain | string | `""` |  |
| alice.services.federated-learning.ports[0].name | string | `"http"` |  |
| alice.services.federated-learning.ports[0].port | int | `8080` |  |
| alice.services.federated-learning.ports[1].name | string | `"ws"` |  |
| alice.services.federated-learning.ports[1].port | int | `8081` |  |
| alice.services.fuseki.ports[0].name | string | `"sparql"` |  |
| alice.services.fuseki.ports[0].port | int | `3030` |  |
| alice.services.gui.ports[0].ingress.clusterIssuer | string | `"letsencrypt"` |  |
| alice.services.gui.ports[0].ingress.path | string | `"/(.*)"` |  |
| alice.services.gui.ports[0].ingress.rewriteTarget | string | `"/$1"` |  |
| alice.services.gui.ports[0].name | string | `"http"` |  |
| alice.services.gui.ports[0].port | int | `80` |  |
| alice.services.worker.ports[0].name | string | `"ws"` |  |
| alice.services.worker.ports[0].port | int | `8777` |  |
| alice.services.worker.ports[1].name | string | `"http"` |  |
| alice.services.worker.ports[1].port | int | `8080` |  |
| broker.brokerDataApp.image | string | `"registry.ids.smart-connected.nl/broker-data-app:2.0.0-develop"` |  |
| broker.ids.daps.url | string | `"http://{{.Release.Name}}-daps:8080"` |  |
| broker.mongodb.auth.rootPassword | string | `"mvm8kf7PTpqJhwrgdJknclCYrL8IkWiF5PfzmmmTbsA"` |  |
| broker.mongodb.enabled | bool | `true` |  |
| daps.mongodb.auth.rootPassword | string | `"mvm8kf7PTpqJhwrgdJknclCYrL8IkWiF5PfzmmmTbsA"` |  |
| daps.mongodb.enabled | bool | `false` |  |
| daps.mongodb.hostname | string | `"fl-mongodb"` |  |
| daps.url | string | `"http://{{.Release.Name}}-daps:8080"` |  |
| mongoDbPassword | string | `"mvm8kf7PTpqJhwrgdJknclCYrL8IkWiF5PfzmmmTbsA"` |  |
