import argparse
import asyncio
import json
import logging
import ssl
import sys
import threading
from datetime import datetime
from io import BytesIO

import numpy as np
import pandas as pd
import syft as sy
import torch
import websockets
from aiohttp import web
from torchvision import datasets, transforms

from websocket_server import WebsocketServerWorker


class DataSetHandler:
    def __init__(self, server: WebsocketServerWorker):
        self.server = server

    async def delete_dataset(self, request):
        key = request.match_info.get('key')
        logger.info("deleting dataset with key {}".format(key))
        self.server.remove_dataset(key=key)
        return web.Response()

    async def list_datasets(self, request):
        json_object = json.dumps(list(self.server.datasets.keys()), indent=4)
        return web.Response(body=json_object)

    async def add_dataset(self, request):
        """returns the metadata of the newly added dataset.
        Add a dataset to the pysyft worker.
        """
        key = request.match_info.get('key')
        logger.info('received dataset with key {}.'.format(key))
        if key in self.server.datasets:
            raise web.HTTPConflict(
                text="Dataset with key %s already exists" % key)

        csv = await request.read()
        dataframe = pd.read_csv(BytesIO(csv))
        X = dataframe.loc[:, dataframe.columns != "label"].values/255
        X = np.asarray(X).reshape(-1, 28, 28)
        mnist_mean = np.mean(X)
        mnist_std = np.std(X)
        Y = dataframe.label.values
        X = torch.Tensor(X)
        Y = torch.LongTensor(Y)
        dataset = sy.BaseDataset(data=X, targets=Y, transform=transforms.Compose(
            [transforms.ToTensor(), transforms.Normalize((mnist_mean,), (mnist_std,))]
        ))
        self.server.add_dataset(dataset, key=key)

        count = [0] * 10
        logger.info(
            "MNIST dataset (%s set), available numbers on %s: ", "train", id)
        for i in range(10):
            count[i] = (dataset.targets == i).sum().item()
            logger.info("      %s: %s", i, count[i])

        logger.info("datasets: %s", self.server.datasets)
        logger.info("len(datasets[mnist]): %s", len(self.server.datasets[key]))
        response_dict = {
            'name': key,
            'shape': list(X.size()),
            'byteSize': sys.getsizeof(csv),
            'mediaType': 'text/csv',
            'creationDate': datetime.now().isoformat()
        }
        return web.json_response(response_dict)


async def start_websocket_server_worker(id, host, port, hook, verbose, loop=None, training=True):
    """Helper function for spinning up a websocket server and setting up the local datasets."""
    server = WebsocketServerWorker(
        id=id, host=host, port=port, hook=hook, verbose=verbose, loop=loop
    )

    if not training:
        mnist_dataset = datasets.MNIST(
            root="./data",
            train=training,
            download=True,
            transform=transforms.Compose(
                [transforms.ToTensor(), transforms.Normalize((0.1307,), (0.3081,))]
            ),
        )
        dataset = sy.BaseDataset(
            data=mnist_dataset.data,
            targets=mnist_dataset.targets,
            transform=mnist_dataset.transform,
        )
        key = "mnist_testing"
        server.add_dataset(dataset, key=key)

        count = [0] * 10
        logger.info(
            "MNIST dataset (%s set), available numbers on %s: ", "train" if training else "test", id
        )
        for i in range(10):
            count[i] = (dataset.targets == i).sum().item()
            logger.info("      %s: %s", i, count[i])

        logger.info("datasets: %s", server.datasets)

    await server.start()
    return server


async def syft():

    # Parse args
    parser = argparse.ArgumentParser(
        description="Run websocket server worker.")
    parser.add_argument(
        "--port",
        "-p",
        type=int,
        help="port number of the websocket server worker, e.g. --port 8777",
    )
    parser.add_argument("--host", type=str, default="localhost",
                        help="host for the connection")
    parser.add_argument(
        "--id", type=str, help="name (id) of the websocket server worker, e.g. --id alice"
    )
    parser.add_argument(
        "--testing",
        action="store_true",
        help="if set, websocket server worker will load the test dataset instead of the training dataset",
    )
    parser.add_argument(
        "--verbose",
        "-v",
        action="store_true",
        help="if set, websocket server worker will be started in verbose mode",
    )
    parser.add_argument(
        "--httpport",
        type=int,
        help="port number of HTTP endpoint",
    )

    args = parser.parse_args()

    # Hook and start server
    hook = sy.TorchHook(torch)

    server = await start_websocket_server_worker(
        id=args.id,
        host=args.host,
        port=args.port,
        hook=hook,
        verbose=args.verbose,
        training=not args.testing,
        loop=asyncio.get_event_loop()
    )
    return server


async def http(server: WebsocketServerWorker):
    # Parse args
    parser = argparse.ArgumentParser(
        description="Run websocket server worker.")
    parser.add_argument(
        "--port",
        "-p",
        type=int,
        help="port number of the websocket server worker, e.g. --port 8777",
    )
    parser.add_argument("--host", type=str, default="localhost",
                        help="host for the connection")
    parser.add_argument(
        "--id", type=str, help="name (id) of the websocket server worker, e.g. --id alice"
    )
    parser.add_argument(
        "--testing",
        action="store_true",
        help="if set, websocket server worker will load the test dataset instead of the training dataset",
    )
    parser.add_argument(
        "--verbose",
        "-v",
        action="store_true",
        help="if set, websocket server worker will be started in verbose mode",
    )
    parser.add_argument(
        "--httpport",
        type=int,
        help="port number of HTTP endpoint",
    )

    args = parser.parse_args()

    dataset_handler = DataSetHandler(server)
    app = web.Application(client_max_size=1024**3)
    app.router.add_post('/datasets/{key}', dataset_handler.add_dataset)
    app.router.add_delete("/datasets/{key}", dataset_handler.delete_dataset)
    app.router.add_get("/datasets", dataset_handler.list_datasets)

    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, '0.0.0.0', args.httpport)
    await site.start()
    logger.info("Started AIOHTTP Server")


async def main():
    server = await syft()
    await http(server)
    while True:
        await asyncio.sleep(60)


if __name__ == "__main__":
    # Logging setup
    FORMAT = "%(asctime)s | %(message)s"
    logging.basicConfig(format=FORMAT)
    logger = logging.getLogger("worker")
    logger.setLevel(level=logging.DEBUG)
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    except KeyboardInterrupt:
        logger.info("Received exit signal, stopping")
