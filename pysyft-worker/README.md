# PySyft Worker
This repository contains python code that enables Federated Learning using PySyft.
Code from [this blog](https://blog.openmined.org/asynchronous-federated-learning-in-pysyft/) is used as basis.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Introduction
The PySyft Worker is a python application that is able to perform FL training on local models.
This application does know nothing of IDS related concepts, all those responsibilities are handled in the *Federated Learning Data App*.

## Building the project
This project is built using Docker; ``docker build -t TAG_NAME .``.

### Local development
The server hosts both a Websocket server and an HTTP server, make sure both are exposed.
The --port parameter sets the websocket port, while --httpport sets the HTTP port:

```
python worker.py --port 8777 --httpport 8080 --id $WORKER_ID --host 0.0.0.0 
```

## Configuration
The PySyft Worker needs some environment variables in order to work properly:

| Key       | Description                    |
| --------- | ------------------------------ |
| WORKER_ID | Unique ID of the PySyft Worker |
