- [NL AIC: Health PoC](#nl-aic-health-poc)
  - [Project structure](#project-structure)
  - [Federated Learning](#federated-learning)
    - [Running the demo](#running-the-demo)
      - [Preparing the datasets (Alice, Bob, Charlie)](#preparing-the-datasets-alice-bob-charlie)
      - [Finding the available datasets (Researcher)](#finding-the-available-datasets-researcher)
        - [Start Federated Learning (Researcher)](#start-federated-learning-researcher)
  - [VODAN data sharing](#vodan-data-sharing)
    - [Running the demo](#running-the-demo-1)
      - [Inserting data (Alice, Bob, Charlie)](#inserting-data-alice-bob-charlie)
      - [Querying data (Researcher)](#querying-data-researcher)
  - [Technical Components](#technical-components)
  - [Known issues](#known-issues)

# NL AIC: Health PoC
This project contains the documentation and code for a Proof of Concept for the Health sector, initiated by the Dutch AI Coalition [(NL-AIC)](https://nlaic.com/).
The PoC contains two main features, Federated Learning and VODAN data sharing, explained below.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Project structure
The PoC consists of several microservices (further explanation [here](#technical-components)).
Some of those services are located as subfolders in this project:
- [data-app](./data-app/README.md) contains the Java/Kotlin code for the IDS data App
- [pysyft-worker](/pysyft-worker/README.md) contains the Python code that enables Federated Learning through PySyft.
- [pysyft-researcher](/pysyft-researcher/README.md) contains the Python code that enables Federated Learning through PySyft.
- [helm-chart](/helm-chart/README.md) contains the Helm code used for deploying the services to a Kubernetes cluster.

## Federated Learning
Federated Learning (FL) allows for distributed Machine Learning without interchanging privacy sensitive data.
PySyft is the library that performs Federated Learning, inspired by [this blog](https://blog.openmined.org/asynchronous-federated-learning-in-pysyft/).

### Running the demo
In this walkthrough, we assume that Alice, Bob, Charlie and Researcher connectors are present.
When referencing one of the connectors, the front-end application is meant.
The following steps have to performed to perform Federated Learning:

#### Preparing the datasets (Alice, Bob, Charlie)
In each of the worker connectors (Alice, Bob and Charlie):
Select some digits in the "Prepare dataset" section and click "*Prepare dataset*".
Wait until the prepared dataset shows up in the *"Datasets"* collection.
This step takes a while, because the following actions are performed:
1. Downloads a MNIST dataset to the connector
2. Uploads the MNIST dataset to the PySyft worker
3. Publishes the dataset metadata to the broker

![prepare-dataset](img/prepare-dataset.png)

#### Finding the available datasets (Researcher)
Click *"Search available datasets"*. This queries the broker for all connectors in the network and extracts the available datasets from each connector.
  
![search-dataset](img/search-dataset.png)

Two datasets (`mnist` and `mnist_2`) are present in this example.
*Alice*, *Bob* and *Charlie* connectors have `mnist` available.

##### Start Federated Learning (Researcher)
Select a dataset and all (or some) connectors to train on.
Federated Learning will start, model accuracy updates take place after each epoch.
![federated-learning](img/federated-learning.png)

## VODAN data sharing
The Virus Outbreak Data Network [(VODAN)](https://www.go-fair.org/implementation-networks/overview/vodan/) is a joint effort to standardize data sharing related to the COVID-19 pandemic.
This PoC shares data according to the [WHO COVID-19 Semantic data model](https://github.com/FAIRDataTeam/WHO-COVID-CRF) using IDS technologies.

### Running the demo
In this walkthrough, we assume that Alice, Bob, Charlie and Researcher connectors are present.
When referencing one of the connectors, the front-end application is meant.
The following steps have to performed to perform VODAN data sharing:

#### Inserting data (Alice, Bob, Charlie)
Inserting data is possible by typing directly in the *Query* field or uploading a file containing *SPARQL*.

#### Querying data (Researcher)
1. Select the connectors on which to query.
2. Filter using the form controls (for gender, symptom date, cough symptoms, etc), or create a manual query in the *Manual query* tab. (The manual query text corresponds to the active filters in the *simple query* tab)
3. Click *"Send Query"* to query the selected connectors. The Researcher data app will:
   1. Retrieve the access uri for each connector through the broker.
   2. Send an IDS message to the retrieved access uri containing the SPARQL query.
   3. Combine the query results and return to the front-end, which shows the results in a table.

![query-result](img/query-result.png)

## Technical Components
The PoC is based on the [IDS reference architecture](https://www.internationaldataspaces.org/wp-content/uploads/2019/03/IDS-Reference-Architecture-Model-3.0.pdf).
Several connectors are deployed on a kubernetes cluster.

![components](img/components.png)

- *Alice, Bob, Charlie*: Federated Learning workers and storage for VODAN related data in RDF format.
- *Researcher*: the Federated Learning coordinator and able to query data in the VODAN workers.

## Known issues
Sometimes it might happen that connectors think they registered themselves correctly to the Broker, while this is not the case.
This breaks inter connectivity between connectors since the broker is the central component responsible for routing to connectors.
This issue is solved by killing (and thus restarting) the `*-core-container` pods of the connectors so they re-register.