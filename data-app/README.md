# Federated Learning Data App
This repository contains a Kotlin application that acts as an IDS Data App.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Introduction
The Federated Learning Data App is used to act as a bridge between the IDS Connector and an external PySyft application used to perform Federated Learning (FL).
The Data App makes it possible to use WebSockets over the IDS network and exposes API endpoints for the front-end.


## Building the project
Gradle is used as build tool, build the project by running ``./gradlew build``.


## Configuration
The Data App needs some YAML configuration values placed in ``/ids/config.yaml`` to work properly:

| Key                              | Description                                                 |
| -------------------------------- | ----------------------------------------------------------- |
| id                               | URN of an ID                                                |
| participant                      | URN of the participant                                      |
| customProperties.client          | ``true`` for a *Worker*, ``false`` for a *Researcher*         |
| customProperties.datasetEndpoint | Endpoint for dataset interaction in the PySyft worker       |
| customProperties.forward         | Endpoint for websocket interaction in the PySyft worker     |
| customProperties.queryEndpoint   | Query Endpoint for SPARQL interaction in the FUSEKI worker  |
| customProperties.updateEndpoint  | Update Endpoint for SPARQL interaction in the FUSEKI worker |
| customProperties.resourcePath    | Path to store and retrieve datasets in the Data App         |
