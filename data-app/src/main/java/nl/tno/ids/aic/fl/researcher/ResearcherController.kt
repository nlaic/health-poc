package nl.tno.ids.aic.fl.researcher

import de.fraunhofer.iais.eis.InvokeOperationMessage
import de.fraunhofer.iais.eis.InvokeOperationMessageBuilder
import de.fraunhofer.iais.eis.ParameterAssignmentBuilder
import de.fraunhofer.iais.eis.util.Util
import io.javalin.Javalin
import io.javalin.websocket.WsConnectContext
import nl.tno.ids.base.BrokerHandler
import nl.tno.ids.base.HttpHelper
import nl.tno.ids.base.IDSRestController
import nl.tno.ids.base.MessageHandler
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.util.thread.QueuedThreadPool
import org.json.JSONObject
import org.slf4j.LoggerFactory
import java.net.URI
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors

data class Worker(val id: String, val idsid: String, val port: Int?, val url: String?)
data class QueryParameters(val workers: List<Worker>, val query: String)

class ResearcherController {
  private val app: Javalin
  private val sessions = ConcurrentHashMap<String, Pair<WsConnectContext, String>>()

  init {
    LOG.info("Starting websocket server on port 8081")
    app = startWebSocket()
  }

  private fun startWebSocket(): Javalin {
    return Javalin.create { config ->
      config.wsFactoryConfig {
        it.policy.maxBinaryMessageSize = 1 * 1024 * 1024 * 1024 // 1GB
        it.policy.maxTextMessageSize = 1 * 1024 * 1024 * 1024 // 1GB
      }
      config.server { Server(QueuedThreadPool(10, 2, 60_000)) }
    }.apply {
      ws("/:forwardTo", ResearcherWsHandler(sessions))
    }.start(8081)
  }

  fun createMessageHandler(): MessageHandler<InvokeOperationMessage> {
    return ResearcherMessageHandler(sessions)
  }

  companion object {
    private val LOG = LoggerFactory.getLogger(ResearcherController::class.java)
  }
}