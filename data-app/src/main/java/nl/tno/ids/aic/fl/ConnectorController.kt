package nl.tno.ids.aic.fl

import nl.tno.ids.base.BrokerHandler
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

data class DatasetWithConnectors(val datasetName: String, val connectors: List<String>)

@RestController
@RequestMapping("api")
class ConnectorController {
  /**
   * Retrieve all datasets known by the broker, along with the connectors that have the dataset available
   */
  @GetMapping("connectors/datasets")
  fun listConnectorsWithDatasets(): List<DatasetWithConnectors> {
    LOG.info("Received datasets query")
    val query = "PREFIX ids: <https://w3id.org/idsa/core/>\nDESCRIBE * WHERE {\n GRAPH ?g { \n ?s ?o ?p.\n }\n}"
    val connectors = BrokerHandler.getInstance().getConnectors(query)

    // We retrieve a list of connectors, each containing a collection of datasets.
    // But we want to show a list of datasets with relevant connectors.
    val connectorsDatasetPairs: MutableList<Pair<String, String>> = mutableListOf()
    for ((connectorId, connector) in connectors) {
      val connectorDatasets = connector.catalog?.offer?.map { offer -> offer.title[0].value } ?: emptyList()
      for (dataset in connectorDatasets) {
        connectorsDatasetPairs.add(Pair(dataset, connectorId))
      }
    }
    return connectorsDatasetPairs.groupBy { pair -> pair.first }.map { group ->
      DatasetWithConnectors(group.key, group.value.map{ it.second})
    }.toList()
  }

  /**
   * Retrieve a list of connectors known by the broker
   */
  @GetMapping("connectors")
  fun listConnectors(): List<String> {
    LOG.info("Received connectors query")
    val query = "PREFIX ids: <https://w3id.org/idsa/core/>\nDESCRIBE * WHERE {\n GRAPH ?g { \n ?s ?o ?p.\n }\n}"
    val connectors = BrokerHandler.getInstance().getConnectors(query)
    return connectors.map { connector -> connector.key }
  }

  companion object {
    private val LOG = LoggerFactory.getLogger(ConnectorController::class.java)
  }
}