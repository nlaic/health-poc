package nl.tno.ids.aic.fl

import java.util.*
import javax.xml.datatype.XMLGregorianCalendar

class DatasetMetadata {
  val name: String? = null
  val shape: ArrayList<Int>? = null
  val byteSize: Int? = null
  val mediaType: String? = null
  val creationDate: XMLGregorianCalendar? = null
}