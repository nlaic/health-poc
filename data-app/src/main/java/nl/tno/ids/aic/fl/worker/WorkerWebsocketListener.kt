package nl.tno.ids.aic.fl.worker

import de.fraunhofer.iais.eis.RejectionMessage
import nl.tno.ids.aic.fl.IDSController
import nl.tno.ids.base.IDSRestController
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import okio.ByteString
import org.json.JSONObject
import org.slf4j.LoggerFactory
import java.util.Base64
import java.util.concurrent.ConcurrentHashMap

class WorkerWebsocketListener(
    private val sessions: ConcurrentHashMap<String, WebSocket>, 
    private val sessionId: String,
    private val issuerConnector: String) : WebSocketListener() {
  companion object {
    private val LOG = LoggerFactory.getLogger(WorkerWebsocketListener::class.java)
  }

  override fun onOpen(webSocket: WebSocket, response: Response) {
    LOG.info("Opened connection: $sessionId")
    sessions[sessionId] = webSocket
  }

  override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
    LOG.debug("Received binary message")
    IDSRestController.sendHTTP(
        issuerConnector,
        IDSController.headerBuilder("binarymessage", sessionId, issuerConnector),
        Base64.getEncoder().encodeToString(bytes.toByteArray())
    ).let { result ->
      if (result.second.header is RejectionMessage) {
        LOG.error("IDS Rejection: ${(result.second.header as RejectionMessage).rejectionReason}")
      }
    }
  }

  override fun onMessage(webSocket: WebSocket, text: String) {
    LOG.debug("Received text message")
    IDSRestController.sendHTTP(
        issuerConnector,
        IDSController.headerBuilder("message", sessionId, issuerConnector),
        text
    ).let { result ->
      if (result.second.header is RejectionMessage) {
        LOG.error("IDS Rejection: ${(result.second.header as RejectionMessage).rejectionReason}")
      }
    }
  }

  override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
    LOG.info("Websocket closed: $sessionId $code $reason")
    if (code != 1006) {
      IDSRestController.sendHTTP(
          issuerConnector,
          IDSController.headerBuilder("close", sessionId, issuerConnector),
          JSONObject().put("code", code).put("reason", reason).toString()
      ).let { result ->
        if (result.second.header is RejectionMessage) {
          LOG.error("IDS Rejection: ${(result.second.header as RejectionMessage).rejectionReason}")
        }
      }
    }
  }

  override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
    LOG.error("Websocket failure: ", t)
  }
}