package nl.tno.ids.aic.fl

import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils
import org.slf4j.LoggerFactory
import javax.servlet.http.HttpServletResponse

class DataAppUtils {
  companion object {
    private val LOG = LoggerFactory.getLogger(DataAppUtils::class.java)

    /**
     * Utility function to execute a request and it's response to fill the provided response
     */
    fun executeRequestAndFillResponse(request: HttpUriRequest, responseToFill: HttpServletResponse): String {
      val httpClient = HttpClients.custom().disableCookieManagement().build()
      httpClient.execute(request).use { workerResponse ->
        LOG.info("Worker responded with status code ${workerResponse.statusLine.statusCode}")
        responseToFill.status = workerResponse.statusLine.statusCode
        workerResponse.allHeaders.forEach {
          responseToFill.addHeader(it.name, it.value)
        }
        if (workerResponse.entity != null) {
          val workerResponseBody = EntityUtils.toString(workerResponse.entity)
          responseToFill.writer.write(workerResponseBody)
          return workerResponseBody
        }
      }
      return ""
    }
  }
}
