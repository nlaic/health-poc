package nl.tno.ids.aic.fl

import de.fraunhofer.iais.eis.InvokeOperationMessage
import de.fraunhofer.iais.eis.InvokeOperationMessageBuilder
import de.fraunhofer.iais.eis.Message
import de.fraunhofer.iais.eis.ParameterAssignmentBuilder
import de.fraunhofer.iais.eis.RejectionReason
import de.fraunhofer.iais.eis.util.Util.asList
import nl.tno.ids.aic.fl.researcher.ResearcherController
import nl.tno.ids.aic.fl.worker.WorkerMessageHandler
import nl.tno.ids.base.IDSRestController
import nl.tno.ids.base.ResponseBuilder
import nl.tno.ids.common.multipart.MultiPart
import nl.tno.ids.common.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.Config
import nl.tno.ids.common.serialization.DateUtil
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.bind.annotation.RestController
import java.net.URI
import java.net.URLDecoder
import java.net.URLEncoder
import java.util.concurrent.ConcurrentHashMap
import javax.annotation.PostConstruct

@EnableScheduling
@RestController
@EnableAutoConfiguration(exclude = [DataSourceAutoConfiguration::class, HibernateJpaAutoConfiguration::class])
open class IDSController : IDSRestController() {

  /**
   * A boolean in the Federated Learning decides if this DataApp is a Worker or Researcher
   */
  @PostConstruct
  fun init() {
    val client = Config.dataApp().getCustomProperties(FederatedLearningConfig::class.java).client
    if (client) {
      registerMessageHandler(WorkerMessageHandler(ConcurrentHashMap()))
    } else {
      registerMessageHandler(ResearcherController().createMessageHandler())
    }
  }

  companion object {
    fun createRejectionEntity(message: Message?, reason: RejectionReason?): ResponseEntity<String>? {
      return MultiPart.toResponseEntity(MultiPartMessage.Builder()
          .setHeader(ResponseBuilder.createRejectionMessage(message, reason).build())
          .build(), HttpStatus.OK)
    }

    fun createMessageProcessedNotificationEntity(message: Message?): ResponseEntity<String>? {
      return MultiPart.toResponseEntity(MultiPartMessage.Builder()
          .setHeader(ResponseBuilder.createMessageProcessedNotification(message).build())
          .build(), HttpStatus.OK)
    }

    fun headerBuilder(operation: String, sessionId: String, recipientConnector: String): InvokeOperationMessage {
      return InvokeOperationMessageBuilder()
          ._modelVersion_("3.0.0")
          ._operationReference_(URI.create("urn:fl:$operation"))
          ._operationParameterization_(ParameterAssignmentBuilder()
              ._parameter_(sessionIdToUri(sessionId))
              .build())
          ._issued_(DateUtil.now())
          ._issuerConnector_(URI.create(Config.dataApp().id))
          ._senderAgent_(URI.create(Config.dataApp().participant))
          ._recipientAgent_(asList(URI.create(Config.dataApp().participant)))
          ._recipientConnector_(asList(URI.create(recipientConnector)))
          .build()
    }

    private fun sessionIdToUri(sessionId: String): URI {
      return URI.create("urn:fl:sessionid:${URLEncoder.encode(sessionId, Charsets.UTF_8.toString())}")
    }

    fun uriToSessionId(sessionId: URI): String {
      return URLDecoder.decode(sessionId.toString().substring(17), Charsets.UTF_8.toString())
    }
  }
}